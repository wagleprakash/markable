import glob
import os
import shutil
from multiprocessing.pool import Pool
import contextlib
import numpy as np
from time import time
import cv2

image_folder = input('Please Enter Input Folder Path: ')
numberOfCore = input('Please enter number of Core 1 or 8: ')
image_dictionary={}
key_list=[]
#image_folder="/Users/prakashwagle/Documents/Projects/Markable/Python/data/"
output = image_folder+"output"

# The following code would create a output folder
if not os.path.exists(output):
    os.makedirs(output)
else:
    shutil.rmtree(output)
    os.makedirs(output)

# Here we parse the input folder to retrieve images and store them in dictionary
for filename in glob.glob(image_folder + '*.jpg'):
    try:
        image_dictionary[os.path.splitext(os.path.basename(filename))[0]] = os.path.normpath(filename)

    except:
        print filename
        pass


key_list = image_dictionary.keys();
item_list = image_dictionary.items();
ts = time()

# Function to subtract background from a photo
def clothidentifier(value):
    filename=value[0]
    img_path=value[1]
    img = cv2.imread(img_path)

    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    mask = np.zeros(img.shape[:2],np.uint8)
    height,width = img.shape[:2]
    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)
    # bgdModel2 = np.zeros((1,65),np.float64)
    # fgdModel2 = np.zeros((1,65),np.float64)

    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    x =(width/2)-70
    y=height-(height/2)
    w=0
    h = 0
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)

    if(w==0):
        rect = ((width/2)-70,(height/2)-150,width-250,height-100)
    else:
        rect = ( x-40, y+h+20,width-250,height-100)

    cv2.grabCut(img,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)
    mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
    img = img*mask2[:,:,np.newaxis]

    # cv2.imshow('image2',img)
    # cv2.waitKey(0)

    img_name = output +"/"+filename +".jpg"
    cv2.imwrite(img_name,img)



    #print item[0]


if __name__ == '__main__':
    if numberOfCore==8:
        with contextlib.closing(Pool(processes=None)) as pool:
            pool.map(clothidentifier, item_list)
            pool.terminate()
    else:
        for item in item_list:
            clothidentifier(item)


print('Took {}s'.format(time() - ts))
