#**Markable**

##*FileList*  
```
 README.md  
 ./Java  
    ./src  
      /main  
        /java  
          /us.markable  
            AmicableNumber.java  
            RunServer.java  
            /domain  
             RequestDTO.java  
             ResponseDTO.java  
            /web.controller  
            ServerController.java  
            /jobs  
                ScheduledTasks.java  
./Python  
  ./src  
    ClothIdentifier.py  
    haarcascade_frontalface_default.xml

```
##**Java**

####Amicable number  
In this function I have used HashMap and HashSet data structure as both have average time complexity for Insertion and Search of O(1)  
In getSumOfProperDivisor function I have store both quotient and divisor in Hashmap as quotient of proper divisor is also a proper divisor.  
This reduces average time complexity of this algorithm from O(n^2) to O(nk)
where n = Total number of numbers under seed  
      k = position of middle proper divisor

####Spring Rest Service  
For implementing Rest Services I have used SpringBootApplication.  
When you run RunServer.java it would download dependencies and start a Tomcat Server which would host ServerController.java and AmicableNumber.java  
It would then call ScheduledTask.java which at every three seconds interval will send
Http POST request to localhost:8080/message  with {missionID and seed} attached in body.  
It's response will be printed in console along with Http Status code

##**Python**

###Assumptions  
* In this project I have used grabcut Algorithm for subtracting the background from the image.  
* For this I try to identify face of the model and consider space below her face as a foreground.  
* This program works well if photo has the models face clearly visible and has a broght color solid color background.  
* I have used Pool of process to improve performance of this algorithm on multicore processor.  

###*How run it*
1. python ClothIdentifier.py  
2. It will as user to input folder path of Input folder  
3. It will as user to input number of core of a processor  
4. An folder named output will be created in input folder were all the output images will be stored.  
```
   eg.
   Please Enter Input Folder Path: "/Users/prakashwagle/Documents/Projects/Markable/Python/data/"
   Please enter number of Core 1 or 8 : 8

```
