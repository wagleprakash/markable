package us.markable.integration.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import us.markable.domain.RequestDTO;
import us.markable.test.AbstarctTest;

/**
 * Created by prakashwagle on 5/28/16.
 */
@WebIntegrationTest("server.port:8000")
public class ServerControllerIntegrationTest extends AbstarctTest {
    @Autowired
    private WebApplicationContext webApplicationContext;
    private RequestDTO requestDTO;
    private RestTemplate restTemplate;
    private ResponseEntity<String> responseEntity;
    private HttpStatus httpStatus;
    private String result;
    private String correctResult;
    private String url ;

    @Before
    public void setUp()
    {
        url="http://localhost:8000/message";
        restTemplate = new RestTemplate();
        requestDTO = new RequestDTO();
        requestDTO.setMissionId(1);
        requestDTO.setSeed(1000);
        correctResult="[{\"number_1\":220,\"number_2\":284}]";
    }

    @Test
    public void getPairsTest()
    {
        responseEntity = restTemplate.postForEntity(url, requestDTO, String.class);
        result = responseEntity.getBody();
        httpStatus= responseEntity.getStatusCode();
        Assert.assertEquals(correctResult,result);
        Assert.assertEquals(HttpStatus.OK,httpStatus);
        System.out.println("Ans: "+responseEntity.getBody());
        System.out.println("Code: "+responseEntity.getStatusCode());
    }

    @Test
    public void getPairConflictTest()
    {
        try {
            responseEntity = restTemplate.postForEntity(url, requestDTO, String.class);
            result = responseEntity.getBody();
            httpStatus= responseEntity.getStatusCode();
        }
        catch (HttpClientErrorException e)
        {
            Assert.assertNull(result);
            Assert.assertEquals(HttpStatus.CONFLICT,e.getStatusCode());
        }

    }
}
