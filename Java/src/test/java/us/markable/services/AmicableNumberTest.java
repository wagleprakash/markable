package us.markable.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import us.markable.AmicableNumber;
import us.markable.test.AbstarctTest;

/**
 * Created by prakashwagle on 5/28/16.
 */
public class AmicableNumberTest extends AbstarctTest{

    @Autowired
    private AmicableNumber amicableNumber ;
    Integer seed;
    String correctResult;

    @Before
    public void setUp()
    {
        seed=1000;
        correctResult="[{\"number_1\":220,\"number_2\":284}]";
    }

    @Test
    public void getAmicalbePair() throws Exception {
       String result = amicableNumber.getAmicalbePair(seed);
        Assert.assertNotNull("Failure : Expected not null object",result);
        Assert.assertEquals(correctResult,result);
    }

}