package us.markable.test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import us.markable.RunServer;

/**
 * Created by prakashwagle on 5/28/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(RunServer.class)
public abstract class AbstarctTest {
}
