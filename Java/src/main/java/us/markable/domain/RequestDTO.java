package us.markable.domain;

/**
 * Created by prakashwagle on 5/18/16.
 */
public class RequestDTO {
    private int missionId;
    private int seed;

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public int getMissionId() {
        return missionId;
    }

    public void setMissionId(int missionId) {
        this.missionId = missionId;
    }

    @Override
    public String toString() {
        return "RequestDTO{" +
                "missionId=" + missionId +
                ", seed=" + seed +
                '}';
    }
}
