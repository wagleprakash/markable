package us.markable.jobs;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import us.markable.domain.RequestDTO;

/**
 * Created by prakashwagle on 5/18/16.
 */
@Component
public class ScheduledTasks {
    private  static int missionId = 0;
    private String url = "http://localhost:8080/message";

    @Scheduled(fixedRate = 3000)
    public void reportCurrentTime() {
        String response=null;
        ResponseEntity<String> entity=null;
        HttpStatus httpstatus=null;
        RequestDTO requestDTO = new RequestDTO();
        Integer seed = (int) (Math.random() * ((20000 - 1000) + 1)) + 1000;
        missionId =(int) (Math.random() * ((2147483647 - 1) + 1)) + 1;

        requestDTO.setMissionId(missionId);
        requestDTO.setSeed(seed);

        RestTemplate restTemplate = new RestTemplate();

             try {
                 entity = restTemplate.postForEntity(url, requestDTO, String.class);

                 httpstatus = entity.getStatusCode();
                 response = entity.getBody();

                 if (httpstatus.equals(HttpStatus.OK)) {
                     System.out.println("HttpStatus: " + httpstatus);
                     System.out.println("ResponseDTO: " + response.toString());
                 }


             }
             catch (HttpClientErrorException e)
             {
                 System.out.println("HttpStatus: "+e.getStatusCode());
             }
             catch (Exception e)
             {
               e.printStackTrace();
             }

    }
}
