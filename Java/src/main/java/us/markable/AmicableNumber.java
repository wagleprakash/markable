package us.markable;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import us.markable.domain.ResponseDTO;

import java.util.*;

/**
 * Created by prakashwagle on 5/17/16.
 */
@Component
public class AmicableNumber {

    HashMap<Integer,Integer> pairMap = new HashMap<Integer, Integer>(); // This will store a number[key] and it's sum of proper divisor[value]

    public String getAmicalbePair(Integer seed)
    {
        Set set = null;
        String setAsJson=new String();
        HashSet<HashSet> amicalbePairSet = new HashSet<HashSet>();

        for (int i=220;i<seed;i++) // As 220 is the least Amicable number we start loop with 220
        {
            int sum = getSumOfProperDivisor(i);
            if (i!=sum)  // Exclude those pairs where sum of it's proper divisor is equal to number itself
            {
                pairMap.put(i, sum);
            }
        }

        set = pairMap.entrySet();
        Iterator iterator = set.iterator();

        while(iterator.hasNext()) { // We iterate through Map to find Amicable pair
            Map.Entry me = (Map.Entry)iterator.next();
            Integer key = (Integer) me.getKey();
            Integer value = (Integer) me.getValue();

            Integer pair = isAmicable(key,value);
            if(pair.equals(0))
            {
                continue;
            }
            else
            {
                HashSet<Integer> temp = new HashSet<Integer>();
                temp.add(key);
                temp.add(pair);
                amicalbePairSet.add(temp); // We use set of set in order avoid duplication of pairs.
            }
        }

       setAsJson = convertHashSetToJson(amicalbePairSet);

        return setAsJson;
    }

    private Integer isAmicable(Integer value,Integer key)  // Inter change the key/value pair to check if they are amicable
    {
            Integer value2 =  pairMap.get(key);

        if (value.equals(value2))
        {
                return key;
        }
        else
        {
                return 0;
        }
    }


    private Integer getSumOfProperDivisor(Integer number)
    {
        Integer sum=0;
        HashSet<Integer> properdivisors = new HashSet<Integer>();

        for(Integer i=2;i<number;i++) // As every number is divisible by 1 we start out loop with number 2
        {
            if(number%i==0)
            {
                if(!properdivisors.contains(i))
                {
                    properdivisors.add(i);      // will add both the divisor & quotient as bot are proper divisor
                    properdivisors.add(number/i);
                }
                else
                {
                    break; // loop will break if we find repeated divisor so as to save computation.
                }
            }
        }

        Iterator<Integer> iterator = properdivisors.iterator();
        while(iterator.hasNext())
        {
           sum +=iterator.next();
        }

        sum+=1; // As 1 is proper divisor of every number we add it into the sum

        return sum;
    }

    private String convertHashSetToJson(HashSet<HashSet> set)
    {
        // Converts HashSet<HashSet> into JSON using Jackson Object Mapper
        String Json=new String();
        ArrayList<ResponseDTO> arrayList = new ArrayList<ResponseDTO>();
        Iterator<HashSet> it = set.iterator();
        try {
            while(it.hasNext())
            {
                Iterator<Integer> innerIterator = it.next().iterator();
                ResponseDTO responseDTO =new ResponseDTO();
                responseDTO.setNumber_1(innerIterator.next());  // As each inner set will have only two number it is iterated twice.
                responseDTO.setNumber_2(innerIterator.next());
                arrayList.add(responseDTO);
            }
            Json = new ObjectMapper().writeValueAsString(arrayList);

        }
        catch (Exception e){e.printStackTrace();}
        return Json;
    }
}
