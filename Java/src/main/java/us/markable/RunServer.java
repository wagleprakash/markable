package us.markable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by prakashwagle on 5/18/16.
 */
@SpringBootApplication
@EnableScheduling
public class RunServer {
    public static void main(String[] args) {
        SpringApplication.run(RunServer.class,args);
    }
}
