package us.markable.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import us.markable.AmicableNumber;
import us.markable.domain.RequestDTO;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.HashSet;

/**
 * Created by prakashwagle on 5/18/16.
 */
@RestController
public class ServerController implements Serializable{

    HashSet<Integer> idSet = new HashSet<Integer>();
    @Autowired
    AmicableNumber amicableNumber ;
    @RequestMapping(value = "/message",method = RequestMethod.POST, consumes = ("application/json"),produces = ("application/json"))
    public ResponseEntity<?> getPairs(@RequestBody RequestDTO requestDTO) throws HttpClientErrorException,NullPointerException
    {
        System.out.println("getpairs controller callled..." + requestDTO);
        String amicablePair = new String();

        if(idSet.contains(requestDTO.getMissionId()))
        {
            return new ResponseEntity<Object>(null,HttpStatus.CONFLICT);
        }
        else
        {
            idSet.add(requestDTO.getMissionId());
        }

        amicablePair = amicableNumber.getAmicalbePair(requestDTO.getSeed());
        return new ResponseEntity(amicablePair,HttpStatus.OK);
    }

}
